#!/usr/bin/perl -w
use strict;
use LWP::UserAgent;
use HTML::TokeParser::Simple;
my $version = "1.1";
my $debug = 0;
my @allowed = ('403'); # we allow 403 as it's an indication for being blocked as bot.

if(!defined $ARGV[0]) {
    print "Usage: $0 <url to following page>\n";
    exit;
}

my $ua = LWP::UserAgent->new(agent => "Fediverse Usertools / Follow checker $version");
$ua->timeout(10);

my $followurl = shift;
my $followpage = '';

my $end = 0;
my $pagecount = 1;
my (@ok, @fail);

while(!$end) {

    print "DEBUG1: Retrieving follow page $followurl?page=$pagecount\n" if($debug > 0);
    my $response = $ua->get($followurl.'?page='.$pagecount);
    if ($response->is_success) {
        $followpage = $response->decoded_content;
    } else {
        print "# Empty page, quitting...\n";
        $end++;
        last;
    }

    print "# Parsing page $pagecount\n";
    my $parser = HTML::TokeParser::Simple->new(\$followpage);

    while ( my $div = $parser->get_tag('div') ) {
        my $class = $div->get_attr('class');
        next unless defined($class);

        if($class =~ /nothing-here/) { # Final page, end here
            print "# Nothing here, ending loop\n" if($debug);
            $end++;
            last;
        } elsif($class eq 'card h-card') {
            my $anchor = $parser->get_tag('a');
            my $href = $anchor->get_attr('href');

            my $userdata = get_user_page($href);
            my ($hostname,$username) = $href =~ /\/\/(.+?)\/(.+)/;
            my $uid = "$username\@$hostname";
            if(defined $userdata->{'og:url'}) {
                print "OK: $href\n" if($debug);
                push(@ok, { url => $href, code => $userdata->{code}, uid => $uid});
            } elsif(defined $userdata->{generator}) {
                print "OK: $href ($userdata->{generator} but that's ok)\n" if($debug);
                push(@ok, { url => $href, code => $userdata->{code}, uid => $uid});
            } elsif(defined $userdata->{description}) { # bloody peertube
                print "OK: $href (just because there is a meta description header...)\n" if($debug);
                push(@ok, { url => $href, code => $userdata->{code}, uid => $uid});
            } elsif(defined $userdata->{special}) { # bloody pleroma
                print "OK: $href (just because $userdata->{special} is special...)\n" if($debug);
                push(@ok, { url => $href, code => $userdata->{code}, uid => $uid});
            } elsif(defined $userdata->{code} and (grep {$userdata->{code} eq $_} @allowed)) {
                my $code = $userdata->{code};
                print "OK: $href ($code but that's ok)\n" if($debug);
                push(@ok, { url => $href, code => $userdata->{code}, uid => $uid});
            } elsif(defined $userdata->{moved}) {
                print "XXX Account has moved: $href ($userdata->{code}) ($uid)\n";
                push(@fail, { url => $href, code => $userdata->{code}, uid => $uid, moved => 'yes' });
            } else {
                print "XXX Failure getting basic data from: $href ($userdata->{code}) ($uid)\n";
                push(@fail, { url => $href, code => $userdata->{code}, uid => $uid });
            }
            sleep 1; # take it easy :)
        }

    }

    ## Next page
    $pagecount++;
    sleep 1; # let's give the server a bit of rest.
}

if($debug) {
    my $okcount = @ok;
    print "OK userpages ($okcount):\n";
    for my $result (@ok) {
        print "> $result->{url} = $result->{code} ($result->{uid})\n";
    }
}

my $failcount = @fail;
print "Failures ($failcount):\n";
for my $result (@fail) {
    print "> $result->{url} = $result->{code} ($result->{uid})\n";
}

sub get_user_page {
    my ($url) = @_;
    my %data;
    my $infopage = '';
    my $response = $ua->get($url);
    if ($response->is_success) {
        $infopage = "\n".$response->decoded_content;
        $data{code} = $response->code;
    } else {
        print "XXX Error retrieving $url: ".$response->status_line."\n" if($debug);
        $data{code} = $response->code;
        return \%data;
    }
    if(!$infopage) {
        print "XXX nothing in $url\n";
        return \%data;
    }
    print "INFO: Parsing user page: $url\n" if($debug);
    print $infopage if($debug > 2);
    if($infopage =~ /pleroma/) { # SRSLY
        $data{special} = 'pleroma';
        return \%data;
    }
    my $parser = HTML::TokeParser::Simple->new(\$infopage);
    while ( my $meta = $parser->get_tag('meta') ) {
        my $property = $meta->get_attr('property');
        my $name = $meta->get_attr('name');
        my $idfield = '';
        if(defined($meta->get_attr('property'))) {
            $idfield = $meta->get_attr('property');
        } elsif(defined($meta->get_attr('name'))) {
            $idfield = $meta->get_attr('name');
        } else {
            next;
        }

        $data{$idfield} = $meta->get_attr('content');
        print "DEBUG2: $idfield -> $data{$idfield}\n" if($debug > 1);
    }
    while( my $div = $parser->get_tag('div')) {
        my $class = $div->get_attr('class');
        next unless defined($class);
        if($class eq 'moved-account-widget') {
            $data{moved} = 'yes';
        }
    }
    return \%data;
}
